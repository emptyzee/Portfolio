from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import XyzBank1
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys


class XyzBank(BasePage):
    def adding_new_customer(self):
        managerlogin = self.browser.find_element(*XyzBank1.MANAGERLOGINBUTTON).click()
        time.sleep(2)
        addnewcustomer = self.browser.find_element(*XyzBank1.ADDCUSTOMER).click()
        firstname = self.browser.find_element(*XyzBank1.FIRSTNAME).send_keys("Alexander")
        lastname = self.browser.find_element(*XyzBank1.LASTNAME).send_keys("Popov")
        postcode = self.browser.find_element(*XyzBank1.POSTCODE).send_keys("114567")
        submitbutton = self.browser.find_element(*XyzBank1.ADDBUTTON).click()
        waittoalert = WebDriverWait(self.browser, 3).until(
            EC.alert_is_present())
        alertaccept = self.browser.switch_to.alert.accept()
        time.sleep(5)