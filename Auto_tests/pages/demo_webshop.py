from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import DemoShop
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class DemoWebShop(BasePage):
    def adding_in_basket(self):
        self.browser.execute_script("window.scrollBy(0, 200);")
        add_button = self.browser.find_element(*DemoShop.ADD_BUTTON).click()
        self.browser.execute_script("window.scrollBy(0, -250);")
        basket =  self.browser.find_element(*DemoShop.BASKET_BUTTON).click()
        self.browser.implicitly_wait(10)
        option = WebDriverWait(self.browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR,'[value="2231467"]'))).click()
    
        
