from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import DemoQa
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from selenium.webdriver.support.ui import Select

class DemQa(BasePage):
    def practice_form(self):# практика, заполнение формы
        self.browser.execute_script("window.scrollBy(0, 300);")
        firstname = self.browser.find_element(*DemoQa.FIRSTNAME).send_keys("Aleksandr")
        lastname = self.browser.find_element(*DemoQa.LASTNAME).send_keys("Beh")
        email = self.browser.find_element(*DemoQa.EMAIL).send_keys("sashaempty2@gmail.com")
        option = self.browser.find_element(*DemoQa.OPTION).click()
        mobile = self.browser.find_element(*DemoQa.MOBILE).send_keys("9779843811")
        dateofb = self.browser.find_element(*DemoQa.DATEOFB).click()
        month = self.browser.find_element(*DemoQa.MONTH).click()
        month3 = self.browser.find_element(*DemoQa.MONTH3).click()
        year = self.browser.find_element(*DemoQa.YEAR).click()
        year1995 = self.browser.find_element(*DemoQa.YEAR1995).click()
        day = self.browser.find_element(*DemoQa.DAY).click()
        hobbies = self.browser.find_element(*DemoQa.HOBBIES).click()
        adrress = self.browser.find_element(*DemoQa.ADRESS).send_keys("Inessa armandr 881")
        submit = self.browser.find_element(*DemoQa.SUBMIT).click()
        time.sleep(10)

    def check_box(self): # чек бокс
        chebox = self.browser.find_element(*DemoQa.CHECKBOX).click()
        time.sleep(10)
 
    def text_box(self): #заполнение формы текстом 
        fullname = self.browser.find_element(*DemoQa.FULLNAME).send_keys("Alex")
        email = self.browser.find_element(*DemoQa.EMAIL).send_keys("sashaempty2@gmail.com")
        currentadres = self.browser.find_element(*DemoQa.CURRENTADRES).send_keys("Inessa armadnr 15")
        permamentadres = self.browser.find_element(*DemoQa.PERMAMENTADRES).send_keys("Inessa amrde 12")
        submit = self.browser.find_element(*DemoQa.SUBMITBUTTON).click()
        time.sleep(10)

    def radio_button(self): # тест кнопок радиобаттонс
        yesradio = self.browser.find_element(*DemoQa.YESRADIO).click()
        impressivebutton = self.browser.find_element(*DemoQa.IMPRESSIVERADIO).click()
        time.sleep(10)

    def web_table(self): # тест заполнения веб формы
        addnewuser = self.browser.find_element(*DemoQa.ADDNEWUSER).click()
        self.browser.implicitly_wait(3)
        firstn = self.browser.find_element(*DemoQa.FIRSTN).send_keys("Alex")
        lastn = self.browser.find_element(*DemoQa.LASTN).send_keys("Behk")
        emaiil = self.browser.find_element(*DemoQa.EMAIIIL).send_keys("sashaempty2@gmail.com")
        age = self.browser.find_element(*DemoQa.AGE).send_keys("25")
        salary = self.browser.find_element(*DemoQa.SALARY).send_keys("100000")
        depart = self.browser.find_element(*DemoQa.DEPARTMENT).send_keys("commercial")
        submit = self.browser.find_element(*DemoQa.SUBMITBUT).click()
        time.sleep(10)

    def button_clicks(self): # даблклик + райтклик + динамик клик
        achains = ActionChains(self.browser)
        elem2 = self.browser.find_element(*DemoQa.DOUBLECLICK)
        achains.double_click(elem2).perform()
        elem1 = self.browser.find_element(*DemoQa.RIGHTCLICK)
        achains.context_click(elem1).perform()
        clickme = self.browser.find_element(*DemoQa.CLICKME).click()
        time.sleep(8)

    def new_user_registr(self): # регистрация юзера
        newuserbut = self.browser.find_element(*DemoQa.NEWUSERBUT).click()
        self.browser.implicitly_wait(4)
        firstname = self.browser.find_element(*DemoQa.FN).send_keys("Alexander")
        lastname = self.browser.find_element(*DemoQa.LN).send_keys("Beh")
        username = self.browser.find_element(*DemoQa.USERNAME).send_keys("emptyzee")
        password = self.browser.find_element(*DemoQa.PSW).send_keys("Fannycem1!")
        time.sleep(15)
        registerbutton = self.browser.find_element(*DemoQa.BUTTONREDISTER).click()
        time.sleep(10)

    def alerts_tete(self): # тест алертов
        usualalertclick = self.browser.find_element(*DemoQa.USUALALERT).click()
        usualalertaccept = self.browser.switch_to.alert.accept()
        alertafter5sec = self.browser.find_element(*DemoQa.ALERTAFTER5SEC).click()
        alert5secaccept = WebDriverWait(self.browser, 6).until(
            EC.alert_is_present())
        alert5secaccpet = self.browser.switch_to.alert.accept()
        confmirmalertclick = self.browser.find_element(*DemoQa.CONFIRMALERT).click()
        confirmalert = self.browser.switch_to.alert.accept()
        prompalertclick = self.browser.find_element(*DemoQa.PROMTALERT).click()
        time.sleep(2)
        promptalert = self.browser.switch_to.alert
        promptalert.send_keys("123")
        promptalert.accept()
        time.sleep(5)

    def modal_alerts(self): # тест модальных окон
        smallmodal = self.browser.find_element(*DemoQa.SMALLMODAL).click()
        self.browser.implicitly_wait(4)
        closesmallmodal = self.browser.find_element(*DemoQa.CLOSESMALLMODAL).click()
        largemodal = self.browser.find_element(*DemoQa.LARGEMODAL).click()
        closelargemodal = self.browser.find_element(*DemoQa.CLOSELARGEMODAL).click()
        time.sleep(3)

    def data_picker(self): # тест датапикера
        selectdateclick = self.browser.find_element(*DemoQa.SELECTDATECLICK).click()
        monthclick = self.browser.find_element(*DemoQa.MONTHCLICK).click()
        monthapril = self.browser.find_element(*DemoQa.MONTHAPRIL).click()
        yearclick = self.browser.find_element(*DemoQa.YEARCLICK).click()
        year1995 = self.browser.find_element(*DemoQa.YEAR195).click()
        day27 = self.browser.find_element(*DemoQa.DAY27).click()
        time.sleep(5)
        selectdatatime = self.browser.find_element(*DemoQa.SELECTDATAANDTIME).click()
        month = self.browser.find_element(*DemoQa.MONTHCLI).click()
        monthapr = self.browser.find_element(*DemoQa.MONTHAPR).click()
        daay = self.browser.find_element(*DemoQa.DAY277).click()
        time1 = self.browser.find_element(*DemoQa.TIME1).click()
        time.sleep(5)

    def progress_bar(self): # не работает
        startbutton = self.browser.find_element(*DemoQa.STARTBUTTON).click()
        waitforanyprocent = WebDriverWait(self.browser, 15).until(
        EC.text_to_be_present_in_element((By.CSS_SELECTOR,"#progressBar>div"),'35%'))
        startbutton = self.browser.find_element(*DemoQa.STARTBUTTON).click()

    def select_something(self): # тест на простой селект
        select = Select(self.browser.find_element(*DemoQa.SELECT1))
        select.select_by_visible_text("Purple")
        time.sleep(5)
        #select.select_by_value('1')

    def book_store(self): # тест магазина книг
        login1 = self.browser.find_element(*DemoQa.LOGIN1).send_keys("emptyzee")
        password1 = self.browser.find_element(*DemoQa.PASSWORD1).send_keys("Fannycem1!")
        buttonlog = self.browser.find_element(*DemoQa.LOGINBUTT).click()
        self.browser.implicitly_wait(4)
        gotostore = self.browser.find_element(*DemoQa.GOTOSTORE).click()
        addbook = self.browser.find_element(*DemoQa.ADDBOOK).click()
        addtobasket = self.browser.find_element(*DemoQa.ADDTOBASKET).click()
        alert3secaccept = WebDriverWait(self.browser, 6).until(
            EC.alert_is_present())
        alertaccept = self.browser.switch_to.alert.accept()
        self.browser.execute_script("window.scrollBy(0, 200);")
        switchtoprofile = self.browser.find_element(*DemoQa.SWAPTOPROFILE).click()
        deletebook = self.browser.find_element(*DemoQa.DELETEBOOK).click()
        confirmdelete = self.browser.find_element(*DemoQa.CONFIRMDELETE).click()
        alert4secaccept = WebDriverWait(self.browser, 6).until(
            EC.alert_is_present())
        allertacept = self.browser.switch_to.alert.accept()
        time.sleep(6)

    def dynamic_properites(self):# тест на ожидание
        enableafter5secwait = WebDriverWait(self.browser, 6).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "#enableAfter")))
        enableafter5sec = self.browser.find_element(*DemoQa.ENABLEAFTER5SEC).click()
        visibleafter5secwait = WebDriverWait(self.browser, 5).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, "#visibleAfter")))
        visibleafter5sec = self.browser.find_element(*DemoQa.VISIBLEAFTER5SEC).click()