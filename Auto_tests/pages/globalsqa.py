from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import GlobalQa1
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys



class GlobalQa(BasePage):
    def sample_page_form(self): #тест html формы
        picture = self.browser.find_element(*GlobalQa1.PROFILEPIC).send_keys("C:/Users/emptyzee/Desktop/пуккее.jpg")
        name = self.browser.find_element(*GlobalQa1.NAME).send_keys("Alex")
        email = self.browser.find_element(*GlobalQa1.EMAIL).send_keys("sashaempty2@gmail.com")
        select1 = Select(self.browser.find_element(*GlobalQa1.SELECTEXP))
        select1.select_by_value("0-1")
        radio1 = self.browser.find_element(*GlobalQa1.RADIOBUTTON1).click()
        radio2 = self.browser.find_element(*GlobalQa1.RADIOBUTTON2).click()
        confirmalert = self.browser.find_element(*GlobalQa1.BUTTONTOALERT).click()
        alertwait1 = WebDriverWait(self.browser, 2).until(
            EC.alert_is_present())
        confirmalertaccept = self.browser.switch_to.alert.accept()
        alertwait2 = WebDriverWait(self.browser, 2).until(
            EC.alert_is_present())
        simplealertaccept = self.browser.switch_to.alert.accept()
        self.browser.execute_script("window.scrollBy(0, 250);")
        textarea = self.browser.find_element(*GlobalQa1.TEXTAREA).send_keys("Good job")
        submitbutton = self.browser.find_element(*GlobalQa1.SUBMITBUTTON).click()
        time.sleep(7)

    def simple_registerform(self):
        registerbutton1 = self.browser.find_element(*GlobalQa1.REGISTERBUTTON1).click()
        waitforelement = WebDriverWait(self.browser, 6).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, "#firstName")))
        firstname = self.browser.find_element(*GlobalQa1.FIRSTNAME).send_keys("Alex")
        lastname = self.browser.find_element(*GlobalQa1.LASTNAME).send_keys("Beh")
        username = self.browser.find_element(*GlobalQa1.USERNAME).send_keys("emptyzee")
        password = self.browser.find_element(*GlobalQa1.PASSWORD).send_keys("fannycem1")
        registerbutton2 = self.browser.find_element(*GlobalQa1.REGISTERBUTTON2).click()
        time.sleep(7)
