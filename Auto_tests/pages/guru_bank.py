from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import MainPageGuru

class GuruBank(BasePage):
    def go_to_first_task(self):
        login = self.browser.find_element(*MainPageGuru.USER_ID).send_keys('mngr383190')
        password = self.browser.find_element(*MainPageGuru.PASSWORD).send_keys('UjAqerU')
    
    def welcome_text(self):
        assert self.is_element_present(*MainPageGuru.WELCOME_TEXT),("Welcome text is not here")

    def first_task_round_two(self):
        assert self.is_element_present(*MainPageGuru.WELCOME_TEXT),("Welcome text is not here")
        welcome_text = self.browser.find_element(*MainPageGuru.WELCOME_TEXT).text
        assert "mngr" in welcome_text, f"not here"
        self.browser.save_screenshot("screenshot.png")

    def go_to_second_taks(self):
        login = self.browser.find_element(*MainPageGuru.USER_ID).send_keys('mngr383133')
        password = self.browser.find_element(*MainPageGuru.PASSWORD).send_keys('UjAqerU')

    def go_to_third_task(self):
        login = self.browser.find_element(*MainPageGuru.USER_ID).send_keys('mngr383190')
        password = self.browser.find_element(*MainPageGuru.PASSWORD).send_keys('UjAqerP2')

    def go_to_forth_task(self):
        login = self.browser.find_element(*MainPageGuru.USER_ID).send_keys('mngr383191')
        password = self.browser.find_element(*MainPageGuru.PASSWORD).send_keys('UjAqerU1')

    def button_click(self):
        button = self.browser.find_element(*MainPageGuru.BUTTON_LOGIN).click()
        self.browser.implicitly_wait(5)
    
    def alert_confirm(self):
        new_window = self.browser.window_handles[0]
        self.browser.switch_to.window(new_window)


