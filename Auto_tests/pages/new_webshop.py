from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import NewShop
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class NewWebShop(BasePage):
    def adding_item(self):
        self.browser.execute_script("window.scrollBy(0, 300);")
        button = self.browser.find_element(*NewShop.BUTTON1).click()
        message = WebDriverWait(self.browser, 7).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR,"#layer_cart h2")))
        c = message.text
        assert c == "Product successfully added to your shopping cart", "no"
        
    
    def go_to_purchase(self):
        self.browser.implicitly_wait(10)
        button1 = self.browser.find_element(*NewShop.BUTTON2).click()
        self.browser.execute_script("window.scrollBy(0, 200);")
    
    def go_to_login(self):
        self.browser.implicitly_wait(10)
        button2 = self.browser.find_element(*NewShop.BUTTON3).click()
        assert self.is_element_present(*NewShop.LOGIN_FORM),("no login form")

    def select_something(self): #тест нажатия на картинку + добавление единиц товара + селект размера
        self.browser.execute_script("window.scrollBy(0, 300);")
        quick_view = self.browser.find_element(*NewShop.QUICK_VIEW).click()
        self.browser.implicitly_wait(10)
        plus = self.browser.find_element(*NewShop.PLUS).click()
        select = self.browser.find_element(*NewShop.SELECTS).click()
        selectm = self.browser.find_element(*NewShop.SELECTM).click()

    def sign_up(self): # ввод емейл и кнопка зарегистрироваться 
        loginin = self.browser.find_element(*NewShop.LOGIN).click()
        self.browser.implicitly_wait(10)
        assert self.is_element_present(*NewShop.EMAIL),("No email form")
        emailform = self.browser.find_element(*NewShop.EMAIL).send_keys("sashaempty2@gmail.com")
        submitemail = self.browser.find_element(*NewShop.SUBMITEMAIL).click()
        
    def sign_up_stage2(self): # тест регистрации
        self.browser.implicitly_wait(6)
        title = self.browser.find_element(*NewShop.OPTION).click()
        firstname = self.browser.find_element(*NewShop.FIRST_NAME).send_keys("Alex")
        lastname = self.browser.find_element(*NewShop.LAST_NAME).send_keys("Beh")
        password = self.browser.find_element(*NewShop.PASSWORD).send_keys("funny1")
        selectday = self.browser.find_element(*NewShop.DAY).click()
        selectday27 = self.browser.find_element(*NewShop.DAY27).click()
        selectmonth = self.browser.find_element(*NewShop.MONTH).click()
        selectmonth4 = self.browser.find_element(*NewShop.MONTH4).click()
        selectyear = self.browser.find_element(*NewShop.YEAR).click()
        selecty1995 = self.browser.find_element(*NewShop.YEAR1995).click()
        news = self.browser.find_element(*NewShop.NEWS).click()
        fn1 = self.browser.find_element(*NewShop.FIRSTNAME1).send_keys("ALex")
        lastn1 = self.browser.find_element(*NewShop.LASTNAME1).send_keys("Beh")
        adres = self.browser.find_element(*NewShop.ADRESS).send_keys("Inessa armandr d8")
        city = self.browser.find_element(*NewShop.CITY).send_keys('Moscow')
        state = self.browser.find_element(*NewShop.STATE).click()
        state1 = self.browser.find_element(*NewShop.STATE1).click()
        postcode = self.browser.find_element(*NewShop.ZIP).send_keys("11746")
        phone = self.browser.find_element(*NewShop.PHONE).send_keys("+79268728629")
        regbut = self.browser.find_element(*NewShop.SUBMITREG).click()
        time.sleep(10)

    def contact_us(self): # отправка сообщения в contact us
        contact_lick = self.browser.find_element(*NewShop.CONTACT_LINK).click()
        subjhead = self.browser.find_element(*NewShop.SUBJECT_HEAD).click()
        subjheadvalue = self.browser.find_element(*NewShop.SUBJECT_HEADVALUE).click()
        emailadr = self.browser.find_element(*NewShop.EMAILADRESS).send_keys("sashaempty2@gmail.com")
        idorder = self.browser.find_element(*NewShop.IDORDER).send_keys("213")
        textarea = self.browser.find_element(*NewShop.TEXTAREA).send_keys("test")
        submitmess = self.browser.find_element(*NewShop.SUBMITMESS).click()
    
    def search_and_addtocart(self): # поиск и выбор продукта и оплата
        search1 = self.browser.find_element(*NewShop.SEARCH).send_keys('Printed Chiffon Dress')
        searchsubm = self.browser.find_element(*NewShop.SEARCHSUBM).click()
        self.browser.execute_script("window.scrollBy(0, 300);")
        self.browser.implicitly_wait(6)
        quickview = self.browser.find_element(*NewShop.QUICKV).click()
        select1 = self.browser.find_element(*NewShop.SELECT1).click()
        selectl= self.browser.find_element(*NewShop.SELECTL).click()
        plus1 = self.browser.find_element(*NewShop.PLUSS1).click()
        colorgreen = self.browser.find_element(*NewShop.COLOR).click()
        addtocart = self.browser.find_element(*NewShop.ADDTOCART).click()
        checkout1 = self.browser.find_element(*NewShop.CHECKOUT).click()
        checkout2 = self.browser.find_element(*NewShop.CHECKOUT2).click()
        time.sleep(10)



