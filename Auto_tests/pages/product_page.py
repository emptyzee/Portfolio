from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import PageProductLocators
import time

class ProductPage(BasePage):
    def add_to_basket(self):
        button = self.browser.find_element(*PageProductLocators.ADD_TO_BASKET).click()
        
    def should_be_message_about_adding(self):
        # Сначала проверяем, что элементы присутствуют на странице
        assert self.is_element_present(*PageProductLocators.PRODUCT_NAME), (
            "Product name is not presented")
        assert self.is_element_present(*PageProductLocators.MESSAGE_ABOUT_ADDING), (
            "Message about adding is not presented")
        # Затем получаем текст элементов для проверки
        product_name = self.browser.find_element(*PageProductLocators.PRODUCT_NAME).text
        message = self.browser.find_element(*PageProductLocators.MESSAGE_ABOUT_ADDING).text
        # Проверяем, что название товара присутствует в сообщении о добавлении
        # Это можно было бы сделать с помощью split() и сравнения строк,
        # Но не вижу необходимости усложнять код
        assert product_name in message, "No product name in the message"

    def should_be_message_basket_total(self):
        # Сначала проверяем, что элементы присутствуют на странице
        assert self.is_element_present(*PageProductLocators.MESSAGE_BASKET_TOTAL), (
            "Message basket total is not presented")
        assert self.is_element_present(*PageProductLocators.PRODUCT_PRICE), (
            "Product price is not presented")
        # Затем получаем текст элементов для проверки
        message_basket_total = self.browser.find_element(*PageProductLocators.MESSAGE_BASKET_TOTAL).text
        product_price = self.browser.find_element(*PageProductLocators.PRODUCT_PRICE).text
        # Проверяем, что цена товара присутствует в сообщении со стоимостью корзины
        assert product_price in message_basket_total, "No product price in the message"


    def guest_cant_see_success_message_after_adding_product_to_basket(self): #Проверяем, что нет сообщения об успехе с помощью is_not_element_present
        assert self.is_not_element_present(*PageProductLocators.MESSAGE_ABOUT_ADDING),("Success message is presented")
        time.sleep(120)
    
    def guest_cant_see_success_message(self): #Проверяем, что нет сообщения об успехе с помощью is_not_element_present
        assert self.is_not_element_present(*PageProductLocators.MESSAGE_ABOUT_ADDING),("Success message is presented")

    def message_disappeared_after_adding_product_to_basket(self): #Проверяем, что нет сообщения об успехе с помощью is_disappeared
        assert self.is_disappeared(*PageProductLocators.MESSAGE_ABOUT_ADDING),("Success message is presented")