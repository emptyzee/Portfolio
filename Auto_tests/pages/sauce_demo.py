from .base_page import BasePage
from selenium.webdriver.common.by import By
from .locators import SauceDemo
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class SacDemo(BasePage):
    def registr_and_add_items(self): # вход + добавление рюкзана + переход в корзину + чекаут + ввод имени и индекса
        username = self.browser.find_element(*SauceDemo.USERNAME).send_keys("standard_user")
        password = self.browser.find_element(*SauceDemo.PASSWORD).send_keys("secret_sauce")
        loginbutton = self.browser.find_element(*SauceDemo.LOGINBUTTON).click()
        plusbackpack = self.browser.find_element(*SauceDemo.ADDBACKPUCK).click()
        cart = self.browser.find_element(*SauceDemo.CART).click()
        checkout = self.browser.find_element(*SauceDemo.CHECKOUT).click()
        firstname = self.browser.find_element(*SauceDemo.FIRSTNAME).send_keys("Alex")
        lastname = self.browser.find_element(*SauceDemo.LASTNAME).send_keys("Beh")
        postal = self.browser.find_element(*SauceDemo.POSTAL).send_keys("117436")
        continue1 = self.browser.find_element(*SauceDemo.CONTINUE).click()
        time.sleep(10)

    def registr_locked_user(self):#вход за заблокированого юзера
        username = self.browser.find_element(*SauceDemo.USERNAME).send_keys("locked_out_user")
        password = self.browser.find_element(*SauceDemo.PASSWORD).send_keys("secret_sauce")
        loginbutton1 = self.browser.find_element(*SauceDemo.LOGINBUTTON).click()
        errormess = self.browser.find_element(*SauceDemo.ERORMES).text
        assert "Sorry, this user has been locked out" in errormess, "No error message"
        time.sleep(10)

