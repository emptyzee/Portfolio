import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.bankproject import XyzBank
from pages.base_page import BasePage


def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()  #pytest -v -m smoke --tb=line --language=en test_main_page.py  zapusk testa


@pytest.mark.smoke
def test_addingnew_customer(browser):
    link = "https://www.globalsqa.com/angularJs-protractor/BankingProject/"
    browser.set_window_size(1920,1080)
    page1 = XyzBank(browser,link)
    page1.open()
    page1.adding_new_customer()