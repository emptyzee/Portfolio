import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.demo_webshop import DemoWebShop
from pages.base_page import BasePage

@pytest.fixture(scope="session")
def browser():
    br = webdriver.Chrome()
    yield br
    br.quit()


def test_first_option(browser):
    link = "http://demowebshop.tricentis.com/"
    page = DemoWebShop(browser,link)
    page.open()
    page.adding_in_basket()