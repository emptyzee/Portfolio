import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.demoqa import DemQa
from pages.base_page import BasePage


def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()  #pytest -v -m smoke --tb=line --language=en test_main_page.py  zapusk testa


def test_practice_form(browser):# практика, заполнение формы
    link = "https://demoqa.com/automation-practice-form"
    browser.set_window_size(1920,1080) # открыть тест на фулл разрешение
    page = DemQa(browser,link)
    page.open()
    page.practice_form()


def test_check_box(browser): # чек бокс
    link = "https://demoqa.com/checkbox"
    page1 = DemQa(browser,link)
    page1.open()
    page1.check_box()


def test_text_box(browser): #заполнение формы текстом 
    link = "https://demoqa.com/text-box"
    page2 = DemQa(browser,link)
    page2.open()
    page2.text_box()


def test_radiobutton(browser):# тест кнопок радиобаттонс
    link = "https://demoqa.com/radio-button"
    page3 = DemQa(browser,link)
    page3.open()
    page3.radio_button()


def test_web_table(browser): # тест заполнения веб формы
    link = "https://demoqa.com/webtables"
    page4 = DemQa(browser,link)
    page4.open()
    page4.web_table()


def test_clicks_buttons(browser): # даблклик + райтклик + динамик клик
    link = "https://demoqa.com/buttons"
    page5 = DemQa(browser,link)
    page5.open()
    page5.button_clicks()


def test_reg_newuser(browser):# регистрация юзера
    link = "https://demoqa.com/login"
    page6 = DemQa(browser,link)
    page6.open()
    page6.new_user_registr()


def test_alert_accepctions(browser): # тест алертов
    link = "https://demoqa.com/alerts"
    page7 = DemQa(browser,link)
    page7.open()
    page7.alerts_tete()


def test_modal_windows(browser): # тест модальных окон
    link = "https://demoqa.com/modal-dialogs"
    page8 = DemQa(browser,link)
    page8.open()
    page8.modal_alerts()


def test_datapicker(browser): # тест датапикера
    link = "https://demoqa.com/date-picker"
    page9 = DemQa(browser,link)
    page9.open()
    page9.data_picker()


def test_select_something(browser):  # тест на простой селект
    link = "https://demoqa.com/select-menu"
    page10 = DemQa(browser,link)
    page10.open()
    page10.select_something()


def test_bookstore(browser): # тест магазина книг
    link = "https://demoqa.com/login"
    browser.set_window_size(1920,1080)
    page11 = DemQa(browser,link)
    page11.open()
    page11.book_store()

@pytest.mark.smoke 
def test_dynamic_properties(browser): # тест на ожидание
    link = "https://demoqa.com/dynamic-properties"
    page12 = DemQa(browser,link)
    page12.open()
    page12.dynamic_properites()