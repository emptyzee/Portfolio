import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.globalsqa import GlobalQa
from pages.base_page import BasePage


def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()  #pytest -v -m smoke --tb=line --language=en test_main_page.py  zapusk testa


def test_sample_htmlform(browser): #тест html формы
    link = "https://www.globalsqa.com/samplepagetest/"
    page1 = GlobalQa(browser,link)
    page1.open()
    page1.sample_page_form()

@pytest.mark.smoke
def test_simpleregistration(browser):
    link = "https://www.globalsqa.com/angularJs-protractor/registration-login-example/#/login"
    page2 = GlobalQa(browser,link)
    page2.open()
    page2.simple_registerform()