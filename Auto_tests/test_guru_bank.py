import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.guru_bank import GuruBank
from pages.base_page import BasePage

def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()


def test_first_task(browser):
    link = "https://www.demo.guru99.com/V4/"
    page = GuruBank(browser, link)
    page.open()
    page.go_to_first_task()
    page.button_click()
    page.first_task_round_two()

def test_second_task(browser):
    link = "https://www.demo.guru99.com/V4/"
    page1 = GuruBank(browser, link)
    page1.open()
    page1.go_to_second_taks()
    page1.button_click()
    page1.welcome_text()
    page1.alert_confirm()

def test_third_task(browser):
    link = "https://www.demo.guru99.com/V4/"
    page2 = GuruBank(browser, link)
    page2.open()
    page2.go_to_third_task()
    page2.button_click()
    page2.welcome_text()
    page2.alert_confirm()

def test_forth_task(browser):
    link = "https://www.demo.guru99.com/V4/"
    page3 = GuruBank(browser, link)
    page3.open()
    page3.go_to_forth_task()
    page3.button_click()
    page3.welcome_text()
    page3.alert_confirm()
#pytest -v --tb=line --language=en test_guru_bank.py - запускаем жтой командой