import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.main_page import MainPage
from pages.login_page import LoginPage


def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()



def test_guest_can_go_to_login_page(browser):
    link = "http://selenium1py.pythonanywhere.com/"
    page = MainPage(browser, link)   # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес 
    page.open()                      # открываем страницу
    page.go_to_login_page()
    
    # запустил этот тест командой - pytest -v --tb=line --language=en test_main_page.py (более понятный вывод строки)

def test_register_form_login_form_and_url(browser): # проверяем форму регистрации
    link = "http://selenium1py.pythonanywhere.com/en-gb/accounts/login/"
    page1 = LoginPage(browser, link)
    page1.open()
    page1.should_be_login_url()
    page1.should_be_login_form()
    page1.should_be_register_form()

