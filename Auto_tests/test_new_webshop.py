import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.new_webshop import NewWebShop
from pages.base_page import BasePage

def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()
    #pytest -v -m smoke --tb=line --language=en test_main_page.py

def test_adding_items(browser): 
    link = "http://automationpractice.com/index.php"
    page = NewWebShop(browser,link)
    page.open()
    page.adding_item()
    page.go_to_purchase()
    page.go_to_login()


def test_view(browser): #тест нажатия на картинку + добавление единиц товара + селект размера
    link = "http://automationpractice.com/index.php"
    page1 = NewWebShop(browser,link)
    page1.open()
    page1.select_something()


def test_login(browser): # форма регистрации
    link = "http://automationpractice.com/index.php"
    page2 = NewWebShop(browser,link)
    page2.open()
    page2.sign_up()
    page2.sign_up_stage2()


def test_contact_us(browser): # форма contact us
    link = "http://automationpractice.com/index.php"
    page3 = NewWebShop(browser,link)
    page3.open()
    page3.contact_us()

@pytest.mark.smoke
def test_search_and_checkout(browser):# поиск и выбор продукта и оплата
    link = "http://automationpractice.com/index.php"
    page4 = NewWebShop(browser,link)
    page4.open()
    page4.search_and_addtocart()
