import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pages.sauce_demo import SacDemo
from pages.base_page import BasePage

def pytest_addoption(parser):
    parser.addoption('--language', action='store', default="en",
                     help="my options: type1 or type 2")

@pytest.fixture(scope="function")
def browser(request):
    user_language = request.config.getoption("language")
    options = Options()
    options.add_experimental_option('prefs', {'intl.accept_languages': user_language})
    browser = webdriver.Chrome(options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()  #pytest -v -m smoke --tb=line --language=en test_main_page.py  zapusk testa


def test_registr_plus_adding_items(browser):# вход + добавление рюкзана + переход в корзину + чекаут + ввод имени и индекса
    link = "https://www.saucedemo.com/"
    page = SacDemo(browser,link)
    page.open()
    page.registr_and_add_items()

@pytest.mark.smoke
def test_locked_user(browser):#вход за заблокированого юзера
    link = "https://www.saucedemo.com/"
    page1 = SacDemo(browser,link)
    page1.open()
    page1.registr_locked_user()